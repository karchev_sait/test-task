module.exports = {
  mode: 'jit',
  purge: [
    './public/**/*.html',
     './src/**/*.{js,jsx,ts,tsx,vue}',
  ],
  theme: {
    colors: {
      'gray-light': '#999999',
      'blue-light': '#E6EDFF',
    }
  },
}
