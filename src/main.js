import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'

import App from './App.vue'
import './assets/tilewind.css'

import routes from './router/routes.js';

import BaseBtn from './components/Base/BaseBtn.vue';
import BaseCard from './components/Base/BaseCard.vue';
import BaseTabs from './components/Base/BaseTabs.vue';
import BaseToggle from './components/Base/BaseToggle.vue';
import BaseBreadCrumbs from './components/Base/BaseBreadCrumbs.vue';

const router = createRouter({
  history: createWebHistory(),
  routes,
})

// Init App
const app = createApp(App);

// Global registration
app
    .component('BaseBtn', BaseBtn)
    .component('BaseTabs', BaseTabs)
    .component('BaseCard', BaseCard)
    .component('BaseToggle', BaseToggle)
    .component('BaseBreadCrumbs', BaseBreadCrumbs);

app.use(router);


app.mount('#app')
