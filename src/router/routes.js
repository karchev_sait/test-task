import Home from '../pages/Home.vue';
import FirstTask from '../pages/FirstTask.vue';
import SecondTask from '../pages/SecondTask.vue';

var routes = [
  {
    path: '/',
    component: Home,
  }, {
    path: '/first-task',
    component: FirstTask,
  }, {
    path: '/second-task',
    component: SecondTask,
  },
]

export default routes;